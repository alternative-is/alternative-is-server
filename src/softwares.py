#!/usr/bin/env python3
# -*- coding : utf-8 -*-

import os
import json
import pathlib
from git import Repo
from fuzzywuzzy import fuzz
from sentence_transformers import SentenceTransformer, util

#CONFIG
SEMANTIC_MODEL_NAME = "sentence-transformers/distiluse-base-multilingual-cased-v2"
DISTANCE_FACTOR = (10, 30, 35, 25, 20) #name, description, categories, alternative-to, easy_to_use
DISTANCE_EASY2USE = (5, 1) #easy to use ; easy to install

MODEL = SentenceTransformer('all-MiniLM-L6-v2')
#MODEL = SentenceTransformer(SEMANTIC_MODEL_NAME)

class Software :
    def __init__(self, **kwargs) :
        self.name           = kwargs['Name']
        self.description    = kwargs['Description']
        self.embed_descr    = MODEL.encode(self.description)
        self.categories     = kwargs['Categories']
        self.alternative_to = kwargs['Alternative-to']
        self.available_for  = kwargs['Available-for']
        self.ease_of_use    = kwargs['Ease-of-use']
        self.links          = kwargs['Links']
        self.capture        = kwargs['Capture']
        self.md_install_instruction = kwargs['md-install-instruction']
        self.easy_to_install = sum(self.available_for.values())/len(self.available_for)

    def __repr__(self) :
        return f"Software<name={self.name}>"

    def distance_score(self, name="", description="", categories=[], alternative_to="", easy_to_use=-1) :
        """ Return a score distance based on many element of Software."""
        dist_name = 0
        dist_description = 0
        dist_categories = 0
        dist_alternative_to = 0
        dist_easy_to_use = 0
        if name != "" :
            dist_name = self.__distance_name(name)
        if description != "" :
            dist_description = self.__distance_description(description)
        if len(categories) != 0 :
            dist_categories = self.__distance_categories(categories)
        if alternative_to != "" :
            dist_alternative_to = self.__distance_alternative_to(alternative_to)
        if easy_to_use != -1 :
            dist_easy_to_use = self.__distance_easy_to_use(easy_to_use)
        print("Name :", dist_name)
        print("Description :", dist_description)
        print("Catégories :", dist_categories)
        print("Aternative à :", dist_alternative_to)
        print("Facilité :", dist_easy_to_use)
        return (
            DISTANCE_FACTOR[0] * dist_name           +
            DISTANCE_FACTOR[1] * dist_description    +
            DISTANCE_FACTOR[2] * dist_categories     +
            DISTANCE_FACTOR[3] * dist_alternative_to +
            DISTANCE_FACTOR[4] * dist_easy_to_use
        )

    def __distance_name(self, name) :
        """ Compute a levenstein distance on the name."""
        return fuzz.ratio(name, self.name)

    def __distance_description(self, description) :
        """ Compute a semantic distance based on AI model on the description."""
        description = MODEL.encode(description)
        return util.semantic_search(description, self.embed_descr)[0][0]['score']

    def __distance_categories(self, categories) :
        """ Compute a mathematical distance to know the order of magnitude on
        similarity categories : number of similar categories/number total
        categories."""
        return fuzz.token_sort_ratio(" ".join(categories),
                                     " ".join(self.categories))

    def __distance_alternative_to(self, alternative_to, threshold=50) :
        """ The best levenstein distance if above the threshold."""
        return max([fuzz.ratio(alternative_to, i) for i in self.alternative_to])

    def __distance_easy_to_use(self, easy_to_use) :
        """ Compute the easy to use from the users want on the capacity to
        install the software and the capacity to use the software."""
        return 1 / (
            abs(easy_to_use - self.ease_of_use) * DISTANCE_EASY2USE[0] +
            abs(easy_to_use - self.easy_to_install) * DISTANCE_EASY2USE[1]
        ) * 100




class SoftwaresList :
    source_softlist = "https://gitlab.com/alternative-is/alternative2-list"
    target_softlist = pathlib.Path("./alternative2-list")
    def __init__(self) :
        self.softwares = []

    def update(self) :
        """ Update software list and properties from repository."""
        if self.target_softlist.exists() :
            repo = Repo(str(self.target_softlist))
            repo.remotes.origin.pull()
        else :
            Repo.clone_from(self.source_softlist, self.target_softlist)

    def load(self) :
        """ Read all file in cloned local repository for immediate use."""
        softwares = [] #in function load
        for soft in self.target_softlist.glob("*/*.json") :
            print(f"[read] Read properties of {soft}.")
            with open(soft, 'r') as file :
                softwares.append(Software(**json.load(file)))
        self.softwares = softwares #apply to SoftwaresList

    def update_and_load(self) :
        self.update()
        self.load()

    def search(self, **kwargs) :
        """ The search algorithm ! All optionnal named-argument :
        - name:str, the application name user want.
        - description:str, a description of application want to do.
        - categories:list, a list of categories of the application.
        - alternative_to:str, the name of application want to replace.
        - easy_to_use:int, the level of difficulty in using the software. """
        scores = [] #(score, software)
        for i in self.softwares :
            scores.append((i.distance_score(**kwargs), i.name))
        return sorted(scores, key=lambda v: v[0])



if __name__ == "__main__" :
    s = SoftwaresList()
    s.update_and_load()
    print(s.softwares)
    print()
    result = s.search(description="Logiciel de traitement de texte",
                      categories=["Bureautique", "Texte", "Éditeur"],
                      easy_to_use=1)
    print(result)
    print()
    result = s.search(description="Logiciel pour lire de la musique",
                      categories=["Multimédia", "Musique", "Lecteur"],
                      easy_to_use=5)
    print(result)
    print()
    result = s.search(description="Logiciel pour augmenter la qualité des photos",
                      categories=["Multimédia", "Graphisme", "Éditeur"],
                      easy_to_use=0)
    print(result)
    print()
