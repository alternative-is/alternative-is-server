#!/usr/bin/env python3
# -*- coding : utf-8 -*-

from flaskext.markdown import Markdown
from flask import Flask, render_template

app = Flask(__name__)
md_engine = Markdown(
    app,
    extensions=['markdown.extensions.tables', 'markdown.extensions.md_in_html',
                'markdown.extensions.attr_list', 'markdown.extensions.def_list',
                'markdown.extensions.abbr',
                'pymdownx.tasklist', 'pymdownx.arithmatex', 'pymdownx.highlight',
                'pymdownx.inlinehilite', 'pymdownx.keys', 'pymdownx.superfences',
                'pymdownx.progressbar', 'pymdownx.smartsymbols', 'pymdownx.mark',
                'pymdownx.tilde', 'pymdownx.tabbed',
                'legacy_attrs', 'mdx_truly_sane_lists', 'smarty', 'mdx_unimoji',
                'sane_lists', 'nl2br', 'mdx_superscript', 'codehilite',
               ],
    safe_mode=True,
    output_format='html5',
)

@app.route('/')
@app.route('/home.html')
def home() :
    print("\n[accueil] redirect to home")
    return render_template('/home.html')


@app.route('/app/<app_name>', methods=['GET'])
def categories(app_name) :
    print(f"\n[application] {app_name}")
    infos = get_infos()
    return render_template('/application.html',
        name = infos['Name'],
        #icone,
        description = infos['Description'],
        #captures,
        categories = ", ".join(infos['Categories']),
        #remplacement,
        #facilité d'utilisation,
        #facilité d'installation,
        md_install_instruct = infos['md-install-instruction'],
        links = infos['Links']
    )

def get_infos() :
    return {
        'Name': 'Upscayl',
        'Description': "Logiciel pour augmenter la qualité d'une photo, en se basant sur de l’IA.",
        'Categories': ['IA', 'Graphisme', 'Multimédia'],
        'Alternative-to': [],
        'Available-for': {
            'Linux': 2,
            'Windows': 3,
            'MacOS': 2
        },
        'Ease-of-use': 0,
        'Links': {
            'Source': 'https://github.com/upscayl/upscayl'
        },
        'Capture': [
            'https://user-images.githubusercontent.com/25067102/187059440-83f32705-4509-4899-a109-ed2d8248fd2b.png',
            'https://user-images.githubusercontent.com/25067102/187059369-9bc63f1c-e6c0-4d6a-9089-706db43f171f.png',
            'https://user-images.githubusercontent.com/25067102/187059318-2d01a671-53fe-4ecc-9a74-3a791fd55818.png',
            'https://user-images.githubusercontent.com/25067102/187059336-8d6e87ec-232f-4591-89c9-ff451692bcf2.png',
            'https://user-images.githubusercontent.com/25067102/187153200-8e184622-a791-43ad-8d73-e5580034f2f2.png'
        ],
        'md-install-instruction': '\n### Linux :\n1. Go to [releases section](https://github.com/upscayl/upscayl/releases/latest)\n2. Download the .AppImage file.\n3. Right Click AppImage -> Go to Permissions tab -> Check allow file to execute and then double click the file to run Upscayl.\n\n### Mac OS :\n1. Go to [releases section](https://github.com/upscayl/upscayl/releases/latest)\n2. Download the .dmg file.\n3. Double click dmg, drag Upscayl icon into Applications folder.\n\n### Windows :\n1. Go to [releases section](https://github.com/upscayl/upscayl/releases/latest)\n2. Download the .exe file.\n3. Double click exe file, wait for installation, profit.\n'
    }


if __name__ == '__main__' :
    host = "127.0.0.1"
    port = 8080
    app.run(debug=True, host=host, port=port)
