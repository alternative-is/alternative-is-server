# Alternative2

**Looking for an ethical solution ? Find it ! It couldn't be simpler than that !**

(but in progress...)

Alternative2, is an alternative to "Alternative To" website ;)
The service is the same, but with ethical solutions only, and a better search algorithm to better respond to people who don't know what they are looking for...

The particularity is to offer software that has been tested, that is reliable, that we know is maintained, and that will meet the vast majority of users.

## If a software is not in list or need correction :

Go to the software list repository, and follow instructions : [alternative2-list](https://gitlab.com/alternative-is/alternative2-list).



## Install and test actual project...

Project is in progress.

These steps are therefore only for the purpose of testing the current code, and will not provide the service as intended in the future.

**Installation :**

```shell
git clone https://gitlab.com/alternative-is/alternative2-server
cd alternative2-server
virtualenv ./ && source bin/activate
pip install -r requirements.txt
```

**Run :**

```shell
cd src/
#run the actual backend script :
python3 softwares.py
#run the actual frontend script :
python3 server.py
```

software.py file contains the backend code of the application. It manages the list of software, as well as the properties of each software. It also contains the search algorithm.

Now we "just" have to do the front-end...
